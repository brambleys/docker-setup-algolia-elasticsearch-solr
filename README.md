# Solr
https://puratos.atlassian.net/wiki/spaces/TD/pages/2822340654/Solr

This setup will prepare a Docker container running Solr on port 8983. It will create one index called "Products".

### Steps to setup Solr ###
* Change your terminal path to the root of this project
* Execute command: ```docker build -t mysolr solr-input```
* Execute command: ```docker run -p 8983:8983 -t mysolr```
* Push some product data to the "products" index by opening the file solr-input/push_data.sh and click the green play button next to one of the lines. 
  * __!!Run only one of them!!__ They have a different way of storing the data in the index. Combining them will give wrong results when searching! 


# Elastic
https://puratos.atlassian.net/wiki/spaces/TD/pages/2823159866/Elastic+search

### Steps to setup Elastic ### 
* Change your terminal path to the root of this project
* Execute command: ```docker build -t elastic elastic-input```
* Execute command: ```docker run --name elastic -p 9200:9200 -it docker.elastic.co/elasticsearch/elasticsearch:8.0.1```
  * Copy the password and enrollment token from your terminal and keep these in a file somewhere
* Execute command: ```docker cp elastic:/usr/share/elasticsearch/config/certs/http_ca.crt .```
* Push some product data to the "products" index by opening the file elastic-input/push_data.sh
  * Copy + paste the curl command that is in this file

### Steps to install Kibana on top of Elastic
* Execute command: ```docker build -t kibana elastic-input/kibana-input```
* Execute command: ```docker run --name kibana -p 5601:5601 -it docker.elastic.co/kibana/kibana:8.0.1```
* Go to the url that appears in the output of your terminal
* Paste the enrollment token you saved to a file a few steps back
* Login with user ```elastic``` and the password you saved to a file a few steps back

# Query examples

### Fetch all info
Solr: ```curl -X GET 'http://localhost:8983/solr/products/select?debugQuery=false&indent=true&q.op=AND&q=*%3A*&rows=50'```
You can also check this in the UI

Elastic: ```curl -X GET 'https://localhost:9200/products/_search?pretty=true&q=*:*' --cacert http_ca.crt -u elastic -k```
Can be checked in Kibana if wanted ```GET _search?pretty=true&q=*:*```

Algolia: We are better of doing this through a node script instead of using curl for now (will all be done in Java later) You can also check this in the UI.
```
// Execute 'npm i algoliasearch' in terminal first
const algoliasearch = require('algoliasearch');

const client = algoliasearch('91ZKBAM0RM', '8f7dc27dc4b9d2b45b67d4424bf36007');
const index = client.initIndex('poc_puratos');

let hits = [];

index.browseObjects({
  query: '', 
  batch: batch => {
    hits = hits.concat(batch);
  }
}).then(() => console.log(hits));
```

### Fetch all products that have the tag "Muffin"
Solr: ```curl -X GET 'http://localhost:8983/solr/products/select?debugQuery=false&indent=true&q.op=AND&q=tags%3A(Muffin)&rows=50'```
You can also check this in the UI

Elastic: ```curl -X GET 'https://localhost:9200/products/_search?pretty=true&q=tags:(Muffin)' --cacert http_ca.crt -u elastic -k```
Can be checked in Kibana if wanted ```GET _search?pretty=true&q=tags:(Muffin)```

Algolia: We are better of doing this through a node script instead of using curl for now (will all be done in Java later)
You can also check this in the UI.
```
// Execute 'npm i algoliasearch' in terminal first
const algoliasearch = require('algoliasearch');

const client = algoliasearch('91ZKBAM0RM', '8f7dc27dc4b9d2b45b67d4424bf36007');
const index = client.initIndex('poc_puratos');

let hits = [];

index.browseObjects({
query: '',
filters: `tags.title:Muffin`,
batch: batch => {
hits = hits.concat(batch);
}
}).then(() => console.log(hits));
```

